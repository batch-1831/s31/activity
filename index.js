// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allow us to used all the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server Setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.u7e9e.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set the notification for connection or failure
let db = mongoose.connection;

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route;
// localhost:3001/tasks
app.use("/tasks", taskRoute);

// Notify on error
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

/*

    Models
    - contains WHAT objects are needed in our API(ex: users, courses)

    Controllers
    - contains instructions HOW your API will perform its intended tasks
    - Monngoose model queries are used here

    Routes
    - defines WHEN particular controllers will be used
    - a specific controller action will be called when a specific HTTP method is received on a specific API endpoint

    Modules
    - are self-contained units of functionality that can be shared and reused

    ==========================================
    Separation of Concerns
    
    Model folder
     - Contains the Object Schemas and defines the object structure and content

    Controllers folder
     - contain the function and business logic of our JS application
     - Meaning all the operations it can do will be placed here.

    Routes folder
     - contain all the endpoints and assign the http methods for our application (app.get("/"))
     - We separate the routes such that the server/"index.js" only contains information on the server
    
    JS modules
        require => to include a specific module/package
        export.module => to treat a value as a "package" that can be used by other files

    Flow  of exports and require:
    export models . require in controllers
    export controllers > requires in routes
    export routs > require in server (index.js)
*/
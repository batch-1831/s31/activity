const express = require("express");
// The "taskController" allows us to use the function defined inside it.
const taskController = require("../controllers/taskController");
const task = require("../model/task");

// Allow access to HTTP Method middlewares that makes it easier to create routes for our application.
const router = express.Router();

// Route to get all tasks
router.get("/", (req, res) => {
    
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to Create a Task
// localhost:3001/task it is the same with the localhost:3001/task/
router.post("/", (req, res) => {
    // The "createTask" function needs data from the request body, so we need it ot supply in the taskController.createTask(argument);
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to update a task
router.patch("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
// colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the url
// :id is a wildcard where you can put the objectID as a value
// output: localhost:3001/tasks/:id (ex.: localhost:3001/tasks/123456)
router.delete("/:id", (req, res) => {
    // If information will be coming from the URL, the data can be accessed from the request "params" property
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// ============== Activity ==============
// Route to get a specific task
router.get("/:id", (req, res) => {
    taskController.getTask(req.params.id).then(resultId => res.send(resultId));
})

// Route to change the status of a task
router.put("/:id/complete", (req, res) => {
    taskController.updateTaskStatus(req.params.id, req.body).then(newStatus => res.send(newStatus));
})

// Use "module.exports" to export the router object to be used in the server
module.exports = router;